package com.amicly.homeawaydarrankelinske.util;

import android.support.annotation.NonNull;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import static dagger.internal.Preconditions.checkNotNull;

/**
 * Created by darrankelinske on 1/30/18.
 */

public class DateUtil {

    public static String getHumanDate(@NonNull String incomingDate) throws ParseException {
        SimpleDateFormat incomingDateFormat =
                new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.US);
        Date parseDate = incomingDateFormat.parse(incomingDate);
        SimpleDateFormat requiredFormat =
                new SimpleDateFormat("EEEE, MMMM d, yyyy hh:mm aaa", Locale.US);
        return requiredFormat.format(parseDate);
    }
}
