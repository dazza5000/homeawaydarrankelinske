package com.amicly.homeawaydarrankelinske.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by darrankelinske on 1/30/18.
 */

public class Images {

    @SerializedName("huge")
    @Expose
    private String huge;

    public String getHuge() {
        return huge;
    }

    public void setHuge(String huge) {
        this.huge = huge;
    }
}