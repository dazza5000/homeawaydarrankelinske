package com.amicly.homeawaydarrankelinske.events;

import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.amicly.homeawaydarrankelinske.R;
import com.amicly.homeawaydarrankelinske.common.image.GlideApp;
import com.amicly.homeawaydarrankelinske.model.Event;
import com.amicly.homeawaydarrankelinske.util.DateUtil;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.resource.bitmap.CenterCrop;
import com.bumptech.glide.load.resource.bitmap.RoundedCorners;

import java.text.ParseException;
import java.util.List;

import timber.log.Timber;

/**
 * Created by darrankelinske on 1/30/18.
 */

public class EventRecyclerAdapter extends RecyclerView.Adapter<EventRecyclerAdapter.EventViewHolder> {

    private List<Event> events;
    private EventClickListener eventClickListener;

    public EventRecyclerAdapter(List<Event> events, EventClickListener eventClickListener) {
        this.events = events;
        this.eventClickListener = eventClickListener;
    }

    @Override
    public EventViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_event, parent,
                false);
        return new EventViewHolder(view);
    }

    @Override
    public void onBindViewHolder(EventViewHolder holder, int position) {
        final Event event = events.get(position);

        holder.itemView.setOnClickListener(v -> eventClickListener.onClick(event));

        if (event.getPerformers() != null &&
                !TextUtils.isEmpty(event.getPerformers().get(0).getImage())) {
            GlideApp.with(holder.itemView.getContext())
                    .load(event.getPerformers().get(0).getImage())
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .transforms(new CenterCrop(), new RoundedCorners(7))
                    .into(holder.eventImageView);
        } else {
            holder.eventImageView.setImageResource(R.drawable.ic_sentiment_satisfied_24dp);
        }

        holder.eventNameTextView.setText(event.getTitle());
        holder.eventLocationTextView.setText(event.getVenue().getDisplayLocation());
        try {
            holder.eventDateTextView.setText(DateUtil.getHumanDate(event.getDatetimeLocal()));
        } catch (ParseException e) {
            Timber.e(e);
            holder.eventDateTextView.setText(event.getDatetimeLocal());
        }

        if (event.isFavorite()) {
            holder.itemView.setBackgroundColor(holder.itemView.getContext().getResources()
                    .getColor(R.color.colorPrimary));
        } else {
            holder.itemView.setBackgroundColor(holder.itemView.getContext().getResources()
                    .getColor(android.R.color.white));
        }
    }

    @Override
    public int getItemCount() {
        return events.size();
    }

    @Override
    public void onViewRecycled(EventViewHolder holder) {
        super.onViewRecycled(holder);
        GlideApp.with(holder.itemView.getContext()).clear(holder.eventImageView);
    }

    public void setEvents(List<Event> events) {
        this.events = events;
        notifyDataSetChanged();
    }

    public class EventViewHolder extends RecyclerView.ViewHolder {

        private ImageView eventImageView;
        private TextView eventNameTextView;
        private TextView eventLocationTextView;
        private TextView eventDateTextView;

        public EventViewHolder(View itemView) {
            super(itemView);
            eventImageView = itemView.findViewById(R.id.image_view_event_image);
            eventNameTextView = itemView.findViewById(R.id.text_view_event_name);
            eventLocationTextView = itemView.findViewById(R.id.text_view_event_location);
            eventDateTextView = itemView.findViewById(R.id.text_view_event_date);
        }
    }

    public interface EventClickListener {
        void onClick(Event event);
    }
}
