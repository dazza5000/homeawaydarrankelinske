package com.amicly.homeawaydarrankelinske.events;

import com.amicly.homeawaydarrankelinske.base.BasePresenter;
import com.amicly.homeawaydarrankelinske.base.BaseView;
import com.amicly.homeawaydarrankelinske.model.Event;

import java.util.List;

/**
 * Created by darrankelinske on 1/29/18.
 */

public interface EventsContract {
    interface View extends BaseView {
        void showEvents(List<Event> events);
        void showSelectedEvent(Integer eventId);
    }

    interface Presenter extends BasePresenter {
        void searchQueryUpdated(String searchQuery);
        void selectEvent(Event event);
    }
}
