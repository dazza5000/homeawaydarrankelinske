package com.amicly.homeawaydarrankelinske.events;

import com.amicly.homeawaydarrankelinske.common.scheduler.SchedulerProvider;
import com.amicly.homeawaydarrankelinske.data.local.LocalUserEventRepository;
import com.amicly.homeawaydarrankelinske.data.remote.SeatGeekService;
import com.amicly.homeawaydarrankelinske.di.qualifier.Production;
import com.amicly.homeawaydarrankelinske.model.Event;
import com.amicly.homeawaydarrankelinske.model.SearchResult;

import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.subjects.BehaviorSubject;
import timber.log.Timber;

import static dagger.internal.Preconditions.checkNotNull;

/**
 * Created by darrankelinske on 1/29/18.
 */

public class EventsPresenter implements EventsContract.Presenter {

    private EventsContract.View view;
    private SeatGeekService seatGeekService;
    private LocalUserEventRepository localUserEventRepository;
    private SchedulerProvider schedulerProvider;
    private BehaviorSubject<String> searchStringSubject = BehaviorSubject.create();
    private CompositeDisposable behaviorDisposable = new CompositeDisposable();
    private CompositeDisposable apiDisposable = new CompositeDisposable();

    @Inject
    public EventsPresenter(EventsContract.View view, SeatGeekService seatGeekService,
                           LocalUserEventRepository localUserEventRepository,
                           @Production SchedulerProvider schedulerProvider) {
        this.view = checkNotNull(view);
        this.seatGeekService = checkNotNull(seatGeekService);
        this.localUserEventRepository = checkNotNull(localUserEventRepository);
        this.schedulerProvider = checkNotNull(schedulerProvider);
        this.view.setPresenter(this);
    }

    @Override
    public void subscribe() {
        searchStringSubject
                .debounce(200, TimeUnit.MILLISECONDS)
                .filter(charSequence -> charSequence.length() > 1)
                .subscribe(new DisposableObserver<CharSequence>() {
                    @Override
                    protected void onStart() {
                        super.onStart();
                        behaviorDisposable.add(this);
                    }

                    @Override
                    public void onNext(CharSequence charSequence) {
                        performSearch(charSequence.toString());
                    }

                    @Override
                    public void onError(Throwable e) {
                        Timber.e(e);
                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }

    @Override
    public void unSubscribe() {
        behaviorDisposable.clear();
        apiDisposable.clear();
    }

    @Override
    public void searchQueryUpdated(String searchQuery) {
        searchStringSubject.onNext(searchQuery);
    }

    private void performSearch(String searchQuery) {
        apiDisposable.clear();
        seatGeekService.searchEvents(searchQuery)
                .subscribeOn(schedulerProvider.io())
                .observeOn(schedulerProvider.mainThread())
                .subscribe(new DisposableSingleObserver<SearchResult>() {
                    @Override
                    protected void onStart() {
                        super.onStart();
                        apiDisposable.add(this);
                    }

                    @Override
                    public void onSuccess(SearchResult searchResult) {
                        if(!searchResult.getEvents().isEmpty()) {
                            List<Event> events = searchResult.getEvents();
                            Set<String> userFavorites = localUserEventRepository.getFavorites();
                            if (!userFavorites.isEmpty()) {
                                for (Event event : events) {
                                    if (userFavorites.contains(String.valueOf(event.getId()))) {
                                        event.setIsFavorite(true);
                                    }
                                }
                            }
                            view.showEvents(events);
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        Timber.e(e);
                        view.showNotification(e.getMessage());
                    }
                });
    }

    @Override
    public void selectEvent(Event selectedEvent) {
        view.showSelectedEvent(selectedEvent.getId());
    }
}
