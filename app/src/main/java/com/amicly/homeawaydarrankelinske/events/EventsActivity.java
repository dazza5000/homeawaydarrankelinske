package com.amicly.homeawaydarrankelinske.events;

import android.os.Bundle;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.view.Menu;
import android.view.MenuItem;

import com.amicly.homeawaydarrankelinske.R;
import com.amicly.homeawaydarrankelinske.base.BaseActivity;
import com.amicly.homeawaydarrankelinske.eventdetail.EventDetailActivity;
import com.amicly.homeawaydarrankelinske.model.Event;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;


public class EventsActivity extends BaseActivity implements EventsContract.View {

    @Inject
    EventsPresenter homePresenter;

    private EventRecyclerAdapter eventRecyclerAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(com.amicly.homeawaydarrankelinske.R.layout.activity_events);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        RecyclerView eventsRecyclerView = findViewById(R.id.recyclerview_events);
        eventRecyclerAdapter = new EventRecyclerAdapter(new ArrayList<>(),
                event -> { homePresenter.selectEvent(event);});
        eventsRecyclerView.setAdapter(eventRecyclerAdapter);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_events, menu);
        MenuItem searchItem = menu.findItem(R.id.action_search);
        SearchView eventsSearchView = (SearchView) searchItem.getActionView();
        eventsSearchView.setMaxWidth(Integer.MAX_VALUE);
        eventsSearchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                homePresenter.searchQueryUpdated(newText);
                return false;
            }
        });
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public void showEvents(List<Event> events) {
        eventRecyclerAdapter.setEvents(events);
    }

    @Override
    public void showSelectedEvent(Integer eventId) {
        EventDetailActivity.launchDetailActivity(this, eventId);
    }
}
