package com.amicly.homeawaydarrankelinske.eventdetail;

import com.amicly.homeawaydarrankelinske.base.BasePresenter;
import com.amicly.homeawaydarrankelinske.base.BaseView;

/**
 * Created by darrankelinske on 1/31/18.
 */

public interface EventDetailContract {
    interface View extends BaseView {
        void setTitle(String title);
        void setImageUrl(String image);
        void setEventTitle(String title);
        void setEventLocation(String eventLocation);
        void setEventDate(String eventDate);
    }

    interface Presenter extends BasePresenter {
        void loadEventDetails(Integer eventId);
        void favoriteEvent();
    }
}
