package com.amicly.homeawaydarrankelinske.eventdetail;

import android.text.TextUtils;

import com.amicly.homeawaydarrankelinske.common.scheduler.SchedulerProvider;
import com.amicly.homeawaydarrankelinske.data.local.LocalUserEventRepository;
import com.amicly.homeawaydarrankelinske.data.remote.SeatGeekService;
import com.amicly.homeawaydarrankelinske.di.qualifier.Production;
import com.amicly.homeawaydarrankelinske.util.DateUtil;
import com.amicly.homeawaydarrankelinske.model.Event;

import java.text.ParseException;

import javax.inject.Inject;

import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableSingleObserver;
import timber.log.Timber;

import static dagger.internal.Preconditions.checkNotNull;


/**
 * Created by darrankelinske on 1/31/18.
 */

public class EventDetailPresenter implements EventDetailContract.Presenter {

    private EventDetailContract.View view;
    private SeatGeekService seatGeekService;
    private LocalUserEventRepository localUserEventRepository;
    private SchedulerProvider schedulerProvider;
    private CompositeDisposable compositeDisposable = new CompositeDisposable();
    private Integer eventId;

    @Inject
    public EventDetailPresenter(EventDetailContract.View view, SeatGeekService seatGeekService,
                                LocalUserEventRepository localUserEventRepository,
                                @Production SchedulerProvider schedulerProvider) {
        this.view = checkNotNull(view);
        this.seatGeekService = checkNotNull(seatGeekService);
        this.localUserEventRepository = checkNotNull(localUserEventRepository);
        this.schedulerProvider = checkNotNull(schedulerProvider);
        this.view.setPresenter(this);
    }

    @Override
    public void subscribe() {
        if (eventId != null) {
            loadEventDetails(eventId);
        }
    }

    @Override
    public void unSubscribe() {
        compositeDisposable.clear();
    }

    @Override
    public void loadEventDetails(Integer eventId) {
        this.eventId = eventId;
        seatGeekService.getEvent(eventId)
                .subscribeOn(schedulerProvider.io())
                .observeOn(schedulerProvider.mainThread())
                .subscribeWith(new DisposableSingleObserver<Event>() {
                    @Override
                    protected void onStart() {
                        super.onStart();
                        compositeDisposable.add(this);
                    }

                    @Override
                    public void onSuccess(Event event) {
                        view.setTitle(event.getShortTitle());
                        if (event.getPerformers() != null &&
                                !TextUtils.isEmpty(event.getPerformers().get(0).getImage())) {
                            view.setImageUrl(event.getPerformers().get(0).getImage());
                        }

                        view.setEventTitle(event.getTitle());
                        if (event.getVenue() != null) {
                            view.setEventLocation(event.getVenue().getDisplayLocation());
                        }
                        try {
                            view.setEventDate(DateUtil.getHumanDate(event.getDatetimeLocal()));
                        } catch (ParseException e) {
                            Timber.e(e);
                            view.setEventDate(event.getDatetimeLocal());
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        Timber.e(e);
                    }
                });
    }

    @Override
    public void favoriteEvent() {
        localUserEventRepository.favoriteEvent(String.valueOf(eventId));
        view.showNotification("Event Favorited.");
    }
}
