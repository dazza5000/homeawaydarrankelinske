package com.amicly.homeawaydarrankelinske.eventdetail;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.airbnb.lottie.LottieAnimationView;
import com.amicly.homeawaydarrankelinske.R;
import com.amicly.homeawaydarrankelinske.base.BaseActivity;
import com.amicly.homeawaydarrankelinske.common.image.GlideApp;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.resource.bitmap.CenterCrop;
import com.bumptech.glide.load.resource.bitmap.RoundedCorners;

import javax.inject.Inject;

public class EventDetailActivity extends BaseActivity implements EventDetailContract.View {

    private static final String EXTRA_EVENT_ID = "eventId";

    @Inject EventDetailPresenter eventDetailPresenter;

    private Integer eventId;
    private ImageView eventImageView;
    private TextView eventNameTextView;
    private TextView eventLocationTextView;
    private TextView eventDateTextView;
    private LottieAnimationView favoriteAnimationView;

    public static void launchDetailActivity(Context context, Integer eventId) {
        Intent launchIntent = new Intent(context, EventDetailActivity.class);
        launchIntent.putExtra(EXTRA_EVENT_ID, eventId);
        context.startActivity(launchIntent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);

        eventImageView = findViewById(R.id.image_view_event_image);
        eventNameTextView = findViewById(R.id.text_view_event_name);
        eventLocationTextView = findViewById(R.id.text_view_event_location);
        eventDateTextView = findViewById(R.id.text_view_event_date);

        if (getIntent().hasExtra(EXTRA_EVENT_ID)) {
            eventId = getIntent().getIntExtra(EXTRA_EVENT_ID, 0);
        } else {
            finish();
        }

        eventDetailPresenter.loadEventDetails(eventId);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_event_detail, menu);
        MenuItem favoriteItem = menu.findItem(R.id.action_favorite);
        favoriteAnimationView = (LottieAnimationView) favoriteItem.getActionView();
        favoriteAnimationView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                favoriteAnimationView.setSpeed(2f);
                favoriteAnimationView.playAnimation();
                eventDetailPresenter.favoriteEvent();
            }
        });
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public void setTitle(String title) {
        getSupportActionBar().setTitle(title);
    }

    @Override
    public void setImageUrl(String image) {
        GlideApp.with(this)
                .load(image)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .transforms(new CenterCrop(), new RoundedCorners(7))
                .into(eventImageView);
    }

    @Override
    public void setEventTitle(String title) {
        eventNameTextView.setText(title);
    }

    @Override
    public void setEventLocation(String eventLocation) {
        eventLocationTextView.setText(eventLocation);
    }

    @Override
    public void setEventDate(String eventDate) {
        eventDateTextView.setText(eventDate);
    }
}
