package com.amicly.homeawaydarrankelinske.di.qualifier;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import javax.inject.Qualifier;

/**
 * Created by darrankelinske on 2/1/18.
 */

@Qualifier
@Retention(RetentionPolicy.RUNTIME)
public @interface Production {}