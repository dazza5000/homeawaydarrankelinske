package com.amicly.homeawaydarrankelinske.di.module;

import com.amicly.homeawaydarrankelinske.eventdetail.EventDetailActivity;
import com.amicly.homeawaydarrankelinske.di.scope.ActivityScope;
import com.amicly.homeawaydarrankelinske.events.EventsActivity;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

/**
 * Created by darrankelinske on 1/29/18.
 */

@Module
public abstract class ActivityBindingModule {
    @ActivityScope
    @ContributesAndroidInjector(modules = EventsPresenterModule.class)
    abstract EventsActivity eventsActivity();
    
    @ActivityScope
    @ContributesAndroidInjector(modules = EventDetailPresenterModule.class)
    abstract EventDetailActivity eventDetailsActivity();
}
