package com.amicly.homeawaydarrankelinske.di.module;

import com.amicly.homeawaydarrankelinske.eventdetail.EventDetailActivity;
import com.amicly.homeawaydarrankelinske.eventdetail.EventDetailContract;
import com.amicly.homeawaydarrankelinske.di.scope.ActivityScope;

import dagger.Binds;
import dagger.Module;

/**
 * Created by darrankelinske on 1/30/18.
 */

@ActivityScope
@Module
public abstract class EventDetailPresenterModule {
    @Binds
    public abstract EventDetailContract.View provideEventDetailView(EventDetailActivity activity);
}

