package com.amicly.homeawaydarrankelinske.di.module;

import com.amicly.homeawaydarrankelinske.di.scope.ActivityScope;
import com.amicly.homeawaydarrankelinske.events.EventsActivity;
import com.amicly.homeawaydarrankelinske.events.EventsContract;

import dagger.Binds;
import dagger.Module;

/**
 * Created by darrankelinske on 1/29/18.
 */

@ActivityScope
@Module
public abstract class EventsPresenterModule {
    @Binds
    public abstract EventsContract.View provideHomeView(EventsActivity activity);
}
