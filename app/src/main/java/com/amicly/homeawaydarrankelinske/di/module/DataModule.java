package com.amicly.homeawaydarrankelinske.di.module;

/**
 * Created by darrankelinske on 1/29/18.
 */

import android.content.Context;
import android.content.SharedPreferences;

import com.amicly.homeawaydarrankelinske.common.scheduler.AppSchedulerProvider;
import com.amicly.homeawaydarrankelinske.common.scheduler.SchedulerProvider;
import com.amicly.homeawaydarrankelinske.data.remote.SeatGeekService;
import com.amicly.homeawaydarrankelinske.di.qualifier.Production;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.File;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import okhttp3.Cache;
import okhttp3.HttpUrl;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

@Module
public class DataModule {

    public static final String PREFS_NAME = "HomeAwayPreferences";

    @Provides
    @Singleton
    Gson provideGson() {
        return new GsonBuilder().create();
    }

    @Provides
    @Singleton
    OkHttpClient provideOkHttpClient(Context context) {
        File httpCacheDirectory = new File(context.getCacheDir(), "responses");
        int cacheSize = 10 * 1024 * 1024;
        Cache cache = new Cache(httpCacheDirectory, cacheSize);

        return new OkHttpClient.Builder()
                .addInterceptor(new HttpLoggingInterceptor()
                        .setLevel(HttpLoggingInterceptor.Level.BODY))
                .addNetworkInterceptor(new ResponseCacheInterceptor())
                .addInterceptor(new ClientIdInterceptor())
                .cache(cache)
                .connectTimeout(33, TimeUnit.SECONDS)
                .readTimeout(33, TimeUnit.SECONDS)
                .build();
    }

    @Provides
    @Singleton
    Retrofit provideRetrofit(OkHttpClient okHttpClient, Gson gson) {
        return new Retrofit.Builder()
                .client(okHttpClient)
                .baseUrl("https://api.seatgeek.com/2/")
                .addConverterFactory(GsonConverterFactory.create(gson))
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build();
    }

    @Provides
    @Singleton
    SeatGeekService provideSeatGeekService(Retrofit retrofit) {
        return retrofit.create(SeatGeekService.class);
    }

    @Provides
    @Singleton
    SharedPreferences provideSharedPreferences(Context context) {
        return context.getSharedPreferences(PREFS_NAME, 0);
    }

    @Provides
    @Production
    SchedulerProvider provideSchedulerProvider() {
        return new AppSchedulerProvider();
    }

    private class ResponseCacheInterceptor implements Interceptor {
        @Override
        public Response intercept(Chain chain) throws IOException {
            Request originalRequest = chain.request();
            Request.Builder request = originalRequest.newBuilder();
            Response response = chain.proceed(request.build());
            return response.newBuilder()
                    .header("Cache-Control", "public, max-age=7777777")
                    .build();
        }
    }

    private class ClientIdInterceptor implements Interceptor {
        @Override
        public Response intercept(Chain chain) throws IOException {
            Request request = chain.request();
            HttpUrl httpUrl = request.url().newBuilder()
                    .addQueryParameter("client_id","MTA0MjEwODd8MTUxNzI2OTM4My4wNw")
                    .build();
            request = request.newBuilder().url(httpUrl).build();
            return chain.proceed(request);
        }
    }
}