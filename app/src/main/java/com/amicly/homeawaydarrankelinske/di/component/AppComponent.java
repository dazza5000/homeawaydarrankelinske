package com.amicly.homeawaydarrankelinske.di.component;

import android.app.Application;

import com.amicly.homeawaydarrankelinske.common.HomeAwayApplication;
import com.amicly.homeawaydarrankelinske.di.module.ActivityBindingModule;
import com.amicly.homeawaydarrankelinske.di.module.ApplicationModule;
import com.amicly.homeawaydarrankelinske.di.module.DataModule;

import javax.inject.Singleton;

import dagger.BindsInstance;
import dagger.Component;
import dagger.android.AndroidInjector;
import dagger.android.support.AndroidSupportInjectionModule;

/**
 * Created by darrankelinske on 1/29/18.
 */

@Singleton
@Component(modules = {AndroidSupportInjectionModule.class, ActivityBindingModule.class,
        ApplicationModule.class, DataModule.class})
public interface AppComponent extends AndroidInjector<HomeAwayApplication> {

    @Component.Builder
    abstract class Builder extends AndroidInjector.Builder<HomeAwayApplication> {

        @BindsInstance
        public abstract Builder application(Application application);

        public abstract Builder dataModule(DataModule dataModule);

        public abstract AppComponent build();
    }
}
