package com.amicly.homeawaydarrankelinske.common.image;

import com.bumptech.glide.annotation.GlideModule;
import com.bumptech.glide.module.AppGlideModule;

/**
 * Created by darrankelinske on 1/8/18.
 */

@GlideModule
public final class HomeAwayGlideModule extends AppGlideModule {}