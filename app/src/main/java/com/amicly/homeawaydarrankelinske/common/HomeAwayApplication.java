package com.amicly.homeawaydarrankelinske.common;

import com.amicly.homeawaydarrankelinske.di.component.DaggerAppComponent;
import com.amicly.homeawaydarrankelinske.di.module.DataModule;

import dagger.android.AndroidInjector;
import dagger.android.support.DaggerApplication;
import timber.log.Timber;

/**
 * Created by darrankelinske on 1/29/18.
 */

public class HomeAwayApplication extends DaggerApplication {
    @Override
    public void onCreate() {
        super.onCreate();
        Timber.plant(new Timber.DebugTree());
    }

    @Override
    protected AndroidInjector<? extends DaggerApplication> applicationInjector() {
        return DaggerAppComponent.builder()
                .application(this)
                .dataModule(new DataModule())
                .create(this);
    }
}
