package com.amicly.homeawaydarrankelinske.base;

/**
 * Created by darrankelinske on 1/29/18.
 */

public interface BasePresenter {
    void subscribe();
    void unSubscribe();
}
