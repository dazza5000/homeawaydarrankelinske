package com.amicly.homeawaydarrankelinske.base;

/**
 * Created by darrankelinske on 1/29/18.
 */

public interface BaseView<P extends BasePresenter> {
    void showNotification(String s);
    void setPresenter(P presenter);
}
