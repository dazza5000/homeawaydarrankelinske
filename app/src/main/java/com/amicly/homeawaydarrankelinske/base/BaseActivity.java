package com.amicly.homeawaydarrankelinske.base;

import android.annotation.SuppressLint;
import android.widget.Toast;

import com.amicly.homeawaydarrankelinske.events.EventsContract;

import dagger.android.support.DaggerAppCompatActivity;

/**
 * Created by darrankelinske on 2/1/18.
 */

@SuppressLint("Registered")
public class BaseActivity extends DaggerAppCompatActivity implements BaseView {

    BasePresenter presenter;

    @Override
    protected void onResume() {
        super.onResume();
        presenter.subscribe();
    }

    @Override
    protected void onPause() {
        super.onPause();
        presenter.unSubscribe();
    }

    @Override
    public void setPresenter(BasePresenter presenter) {
        this.presenter = presenter;
    }

    @Override
    public void showNotification(String notification) {
        Toast.makeText(this, notification, Toast.LENGTH_SHORT).show();
    }
}
