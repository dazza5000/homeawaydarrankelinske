package com.amicly.homeawaydarrankelinske.data;

import java.util.Set;

/**
 * Created by darrankelinske on 1/30/18.
 */

public interface UserEventRepository {
    void favoriteEvent(String eventId);
    Set<String> getFavorites();
}
