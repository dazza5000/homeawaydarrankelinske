package com.amicly.homeawaydarrankelinske.data.local;

import android.content.SharedPreferences;

import com.amicly.homeawaydarrankelinske.data.UserEventRepository;

import java.util.HashSet;
import java.util.Set;

import javax.inject.Inject;

/**
 * Created by darrankelinske on 1/31/18.
 */

public class LocalUserEventRepository implements UserEventRepository {

    private static final String KEY_EVENT_FAVORITES = "userEventFavorites";

    private SharedPreferences sharedPreferences;

    @Inject
    public LocalUserEventRepository(SharedPreferences sharedPreferences) {
        this.sharedPreferences = sharedPreferences;
    }

    @Override
    public void favoriteEvent(String eventId) {
        HashSet<String> setToSave = new HashSet<>();
        setToSave.addAll(getFavorites());
        setToSave.add(eventId);
        sharedPreferences.edit().putStringSet(KEY_EVENT_FAVORITES, setToSave).apply();
    }

    @Override
    public Set<String> getFavorites() {
        return sharedPreferences.getStringSet(KEY_EVENT_FAVORITES, new HashSet<>());
    }
}
