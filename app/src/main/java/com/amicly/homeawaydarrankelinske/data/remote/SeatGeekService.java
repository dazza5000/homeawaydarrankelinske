package com.amicly.homeawaydarrankelinske.data.remote;

import com.amicly.homeawaydarrankelinske.model.Event;
import com.amicly.homeawaydarrankelinske.model.SearchResult;

import io.reactivex.Single;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by darrankelinske on 1/29/18.
 */

public interface SeatGeekService {
    @GET("events/{EVENT_ID}")
    Single<Event> getEvent(@Path("EVENT_ID") Integer eventId);

    @GET("events")
    Single<SearchResult> searchEvents(@Query("q") String query);
}
