package com.amicly.homeawaydarrankelinske.eventdetail;

import com.amicly.homeawaydarrankelinske.common.scheduler.SchedulerProvider;
import com.amicly.homeawaydarrankelinske.common.scheduler.TrampolineSchedulerProvider;
import com.amicly.homeawaydarrankelinske.data.local.LocalUserEventRepository;
import com.amicly.homeawaydarrankelinske.data.remote.SeatGeekService;
import com.amicly.homeawaydarrankelinske.model.Event;
import com.amicly.homeawaydarrankelinske.model.Venue;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.Collections;

import io.reactivex.Single;

import static org.junit.Assert.assertNotNull;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Created by darrankelinske on 2/3/18.
 */

public class EventDetailPresenterTest {

    private static Event TEST_EVENT;
    private static Venue TEST_VENUE;

    @Mock
    EventDetailContract.View view;
    @Mock
    SeatGeekService seatGeekService;
    @Mock
    LocalUserEventRepository localUserEventRepository;
    SchedulerProvider schedulerProvider = new TrampolineSchedulerProvider();


    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);

        TEST_EVENT = new Event();
        TEST_EVENT.setTitle("Texas Rangers");
        TEST_EVENT.setShortTitle("Texas");
        TEST_EVENT.setDatetimeLocal("2018-02-25T20:00:00");

        TEST_VENUE = new Venue();
        TEST_VENUE.setDisplayLocation("Yeehaw");
        TEST_EVENT.setVenue(TEST_VENUE);

        when(seatGeekService.getEvent(anyInt())).thenReturn(Single.just(TEST_EVENT));
        when(localUserEventRepository.getFavorites()).thenReturn(Collections.emptySet());
    }

    @Test
    public void validPresenterShouldNotBeNull() {
        EventDetailPresenter presenter = new EventDetailPresenter(view, seatGeekService,
                localUserEventRepository, schedulerProvider);

        assertNotNull(presenter);
    }

    @Test(expected = NullPointerException.class)
    public void nullLocalUserRepositoryShouldThrowException() {
        EventDetailPresenter presenter = new EventDetailPresenter(view, seatGeekService,
                null, schedulerProvider);
    }

    @Test
    public void successLoadEventShouldShowEventDetails() {
        EventDetailPresenter presenter = new EventDetailPresenter(view, seatGeekService,
                localUserEventRepository, schedulerProvider);

        presenter.subscribe();
        presenter.loadEventDetails(anyInt());

        verify(view, times(1)).setTitle(TEST_EVENT.getShortTitle());
        verify(view, times(1)).setEventTitle(TEST_EVENT.getTitle());
        verify(view, times(1))
                .setEventLocation(TEST_VENUE.getDisplayLocation());
    }
}


