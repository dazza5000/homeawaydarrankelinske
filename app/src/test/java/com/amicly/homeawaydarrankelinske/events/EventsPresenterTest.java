package com.amicly.homeawaydarrankelinske.events;

import com.amicly.homeawaydarrankelinske.common.scheduler.SchedulerProvider;
import com.amicly.homeawaydarrankelinske.common.scheduler.TrampolineSchedulerProvider;
import com.amicly.homeawaydarrankelinske.data.local.LocalUserEventRepository;
import com.amicly.homeawaydarrankelinske.data.remote.SeatGeekService;
import com.amicly.homeawaydarrankelinske.model.Event;
import com.amicly.homeawaydarrankelinske.model.SearchResult;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import io.reactivex.Single;

import static java.lang.Thread.sleep;
import static org.junit.Assert.assertNotNull;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Created by darrankelinske on 2/3/18.
 */

public class EventsPresenterTest {

    private static List<Event> TEST_EVENTS;
    private static SearchResult TEST_SEARCH_RESULT;

    @Mock
    EventsContract.View view;
    @Mock SeatGeekService seatGeekService;
    @Mock LocalUserEventRepository localUserEventRepository;
    SchedulerProvider schedulerProvider = new TrampolineSchedulerProvider();


    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);

        TEST_EVENTS = Arrays.asList(new Event(), new Event(), new Event());
        TEST_SEARCH_RESULT = new SearchResult();
        TEST_SEARCH_RESULT.setEvents(TEST_EVENTS);

        when(seatGeekService.searchEvents(anyString())).thenReturn(Single.just(TEST_SEARCH_RESULT));
        when(localUserEventRepository.getFavorites()).thenReturn(Collections.emptySet());
    }

    @Test
    public void validPresenterShouldNotBeNull() {
        EventsPresenter presenter = new EventsPresenter(view, seatGeekService,
                localUserEventRepository, schedulerProvider);

        assertNotNull(presenter);
    }

    @Test(expected = NullPointerException.class)
    public void nullLocalUserRepositoryShouldThrowException() {
        EventsPresenter presenter = new EventsPresenter(view, seatGeekService,
                null, schedulerProvider);
    }

    @Test
    public void debounceShouldPreventPrematureSearch() {
        EventsPresenter presenter = new EventsPresenter(view, seatGeekService,
                localUserEventRepository, schedulerProvider);

        presenter.subscribe();
        presenter.searchQueryUpdated("Texas Rangers");

        verify(view, times(0)).showEvents(TEST_EVENTS);
    }

    @Test
    public void searchCompletedShouldShowEvents() {
        EventsPresenter presenter = new EventsPresenter(view, seatGeekService,
                localUserEventRepository, schedulerProvider);

        presenter.subscribe();
        presenter.searchQueryUpdated("Texas Rangers");
        sleepForDebounce();

        verify(view, times(1)).showEvents(TEST_EVENTS);
    }

    private void sleepForDebounce() {
        try {
            sleep(700);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
