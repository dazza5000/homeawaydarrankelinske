package com.amicly.homeawaydarrankelinske.util;

import com.amicly.homeawaydarrankelinske.events.EventsPresenter;

import junit.framework.Assert;

import org.junit.Test;

import java.text.ParseException;

import static org.junit.Assert.assertNotNull;

/**
 * Created by daz on 2/3/18.
 */

public class DateUtilTest {

    @Test(expected = NullPointerException.class)
    public void validIncomingDateShouldNotBeNull() throws ParseException {
        DateUtil.getHumanDate(null);
    }

    @Test
    public void testDateParsing() throws ParseException {
        String incomingDate = "2018-02-25T20:00:00";
        Assert.assertEquals(DateUtil.getHumanDate(incomingDate),
                "Sunday, February 25, 2018 08:00 PM");
    }
}
